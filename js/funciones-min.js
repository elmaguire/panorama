var menuSource=document.getElementById("contenedor-Template").innerHTML,menuTemplate=Handlebars.compile(menuSource);document.getElementById("panoram").innerHTML=menuTemplate(contenidoInteractivo);

(function(window, factory) {
	var lazySizes = factory(window, window.document);
	window.lazySizes = lazySizes;
	if(typeof module == 'object' && module.exports){
		module.exports = lazySizes;
	} else if (typeof define == 'function' && define.amd) {
		define(lazySizes);
	}
}(window, function(window, document) {
	'use strict';
	/*jshint eqnull:true */
	if(!document.getElementsByClassName){return;}

	var lazySizesConfig;

	var docElem = document.documentElement;

	var supportPicture = window.HTMLPictureElement && ('sizes' in document.createElement('img'));

	var _addEventListener = 'addEventListener';

	var addEventListener = window[_addEventListener];

	var setTimeout = window.setTimeout;

	var rAF = window.requestAnimationFrame || setTimeout;

	var regPicture = /^picture$/i;

	var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];

	var regClassCache = {};

	var forEach = Array.prototype.forEach;

	var hasClass = function(ele, cls) {
		if(!regClassCache[cls]){
			regClassCache[cls] = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		}
		return regClassCache[cls].test(ele.className) && regClassCache[cls];
	};

	var addClass = function(ele, cls) {
		if (!hasClass(ele, cls)){
			ele.className = ele.className.trim() + ' ' + cls;
		}
	};

	var removeClass = function(ele, cls) {
		var reg;
		if ((reg = hasClass(ele,cls))) {
			ele.className = ele.className.replace(reg, ' ');
		}
	};

	var addRemoveLoadEvents = function(dom, fn, add){
		var action = add ? _addEventListener : 'removeEventListener';
		if(add){
			addRemoveLoadEvents(dom, fn);
		}
		loadEvents.forEach(function(evt){
			dom[action](evt, fn);
		});
	};

	var triggerEvent = function(elem, name, detail, noBubbles, noCancelable){
		var event = document.createEvent('CustomEvent');

		event.initCustomEvent(name, !noBubbles, !noCancelable, detail || {});

		elem.dispatchEvent(event);
		return event;
	};

	var updatePolyfill = function (el, full){
		var polyfill;
		if( !supportPicture && ( polyfill = (window.picturefill || lazySizesConfig.pf) ) ){
			polyfill({reevaluate: true, elements: [el]});
		} else if(full && full.src){
			el.src = full.src;
		}
	};

	var getCSS = function (elem, style){
		return (getComputedStyle(elem, null) || {})[style];
	};

	var getWidth = function(elem, parent, width){
		width = width || elem.offsetWidth;

		while(width < lazySizesConfig.minSize && parent && !elem._lazysizesWidth){
			width =  parent.offsetWidth;
			parent = parent.parentNode;
		}

		return width;
	};

	var throttle = function(fn){
		var running;
		var lastTime = 0;
		var Date = window.Date;
		var run = function(){
			running = false;
			lastTime = Date.now();
			fn();
		};
		var afterAF = function(){
			setTimeout(run);
		};
		var getAF = function(){
			rAF(afterAF);
		};

		return function(){
			if(running){
				return;
			}
			var delay = 125 - (Date.now() - lastTime);

			running =  true;

			if(delay < 6){
				delay = 6;
			}
			setTimeout(getAF, delay);
		};
	};

	/*
	var throttle = function(fn){
		var running;
		var lastTime = 0;
		var Date = window.Date;
		var requestIdleCallback = window.requestIdleCallback;
		var gDelay = 125;
		var dTimeout = 999;
		var timeout = dTimeout;
		var run = function(){
			running = false;
			lastTime = Date.now();
			fn();
		};
		var afterAF = function(){
			setImmediate(run);
		};
		var getAF = function(){
			rAF(afterAF);
		};

		if(requestIdleCallback){
			gDelay = 99;
			getAF = function(){
				requestIdleCallback(run, timeout);
				if(timeout !== dTimeout){
					timeout = dTimeout;
				}
			};
		}

		return function(isPriority){

			if((isPriority = isPriority === true)){
				timeout = 40;
			}

			if(running){
				return;
			}
			var delay = gDelay - (Date.now() - lastTime);

			running =  true;

			if(isPriority || delay < 0){
				getAF();
			} else {
				setTimeout(getAF, delay);
			}
		};
	};
	*/

	var loader = (function(){
		var lazyloadElems, preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;

		var eLvW, elvH, eLtop, eLleft, eLright, eLbottom;

		var defaultExpand, preloadExpand;

		var regImg = /^img$/i;
		var regIframe = /^iframe$/i;

		var supportScroll = ('onscroll' in window) && !(/glebot/.test(navigator.userAgent));

		var shrinkExpand = 0;
		var currentExpand = 0;

		var isLoading = 0;
		var lowRuns = 0;

		var resetPreloading = function(e){
			isLoading--;
			if(e && e.target){
				addRemoveLoadEvents(e.target, resetPreloading);
			}

			if(!e || isLoading < 0 || !e.target){
				isLoading = 0;
			}
		};

		var isNestedVisible = function(elem, elemExpand){
			var outerRect;
			var parent = elem;
			var visible = getCSS(elem, 'visibility') != 'hidden';

			eLtop -= elemExpand;
			eLbottom += elemExpand;
			eLleft -= elemExpand;
			eLright += elemExpand;

			while(visible && (parent = parent.offsetParent)){
				visible = ((getCSS(parent, 'opacity') || 1) > 0);

				if(visible && getCSS(parent, 'overflow') != 'visible'){
					outerRect = parent.getBoundingClientRect();
					visible = eLright > outerRect.left &&
					eLleft < outerRect.right &&
					eLbottom > outerRect.top - 1 &&
					eLtop < outerRect.bottom + 1
					;
				}
			}

			return visible;
		};

		var checkElements = function() {
			var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal;

			if((loadMode = lazySizesConfig.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)){

				i = 0;

				lowRuns++;

				if(currentExpand < preloadExpand && isLoading < 1 && lowRuns > 3 && loadMode > 2){
					currentExpand = preloadExpand;
					lowRuns = 0;
				} else if(loadMode > 1 && lowRuns > 2 && isLoading < 6){
					currentExpand = defaultExpand;
				} else {
					currentExpand = shrinkExpand;
				}

				for(; i < eLlen; i++){

					if(!lazyloadElems[i] || lazyloadElems[i]._lazyRace){continue;}

					if(!supportScroll){unveilElement(lazyloadElems[i]);continue;}

					if(!(elemExpandVal = lazyloadElems[i].getAttribute('data-expand')) || !(elemExpand = elemExpandVal * 1)){
						elemExpand = currentExpand;
					}

					if(beforeExpandVal !== elemExpand){
						eLvW = innerWidth + elemExpand;
						elvH = innerHeight + elemExpand;
						elemNegativeExpand = elemExpand * -1;
						beforeExpandVal = elemExpand;
					}

					rect = lazyloadElems[i].getBoundingClientRect();

					if ((eLbottom = rect.bottom) >= elemNegativeExpand &&
						(eLtop = rect.top) <= elvH &&
						(eLright = rect.right) >= elemNegativeExpand &&
						(eLleft = rect.left) <= eLvW &&
						(eLbottom || eLright || eLleft || eLtop) &&
						((isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4)) || isNestedVisible(lazyloadElems[i], elemExpand))){
						unveilElement(lazyloadElems[i]);
						loadedSomething = true;
						if(isLoading > 9){break;}
						if(isLoading > 6){currentExpand = shrinkExpand;}
					} else if(!loadedSomething && isCompleted && !autoLoadElem &&
						isLoading < 4 && lowRuns < 4 && loadMode > 2 &&
						(preloadElems[0] || lazySizesConfig.preloadAfterLoad) &&
						(preloadElems[0] || (!elemExpandVal && ((eLbottom || eLright || eLleft || eLtop) || lazyloadElems[i].getAttribute(lazySizesConfig.sizesAttr) != 'auto')))){
						autoLoadElem = preloadElems[0] || lazyloadElems[i];
					}
				}

				if(autoLoadElem && !loadedSomething){
					unveilElement(autoLoadElem);
				}
			}
		};

		var throttledCheckElements = throttle(checkElements);

		var switchLoadingClass = function(e){
			addClass(e.target, lazySizesConfig.loadedClass);
			removeClass(e.target, lazySizesConfig.loadingClass);
			addRemoveLoadEvents(e.target, switchLoadingClass);
		};

		var changeIframeSrc = function(elem, src){
			try {
				elem.contentWindow.location.replace(src);
			} catch(e){
				elem.src = src;
			}
		};

		var handleSources = function(source){
			var customMedia, parent;

			var sourceSrcset = source.getAttribute(lazySizesConfig.srcsetAttr);

			if( (customMedia = lazySizesConfig.customMedia[source.getAttribute('data-media') || source.getAttribute('media')]) ){
				source.setAttribute('media', customMedia);
			}

			if(sourceSrcset){
				source.setAttribute('srcset', sourceSrcset);
			}

			if(customMedia){
				parent = source.parentNode;
				parent.insertBefore(source.cloneNode(), source);
				parent.removeChild(source);
			}
		};

		var rafBatch = (function(){
			var isRunning;
			var batch = [];
			var runBatch = function(){
				while(batch.length){
					(batch.shift())();
				}
				isRunning = false;
			};
			return function(fn){
				batch.push(fn);
				if(!isRunning){
					isRunning = true;
					rAF(runBatch);
				}
			};
		})();

		var unveilElement = function (elem){
			var src, srcset, parent, isPicture, event, firesLoad, width;

			var isImg = regImg.test(elem.nodeName);

			//allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")
			var sizes = isImg && (elem.getAttribute(lazySizesConfig.sizesAttr) || elem.getAttribute('sizes'));
			var isAuto = sizes == 'auto';

			if( (isAuto || !isCompleted) && isImg && (elem.src || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesConfig.errorClass)){return;}

			if(isAuto){
				width = elem.offsetWidth;
			}

			elem._lazyRace = true;
			isLoading++;

			rafBatch(function lazyUnveil(){
				if(elem._lazyRace){
					delete elem._lazyRace;
				}

				removeClass(elem, lazySizesConfig.lazyClass);

				if(!(event = triggerEvent(elem, 'lazybeforeunveil')).defaultPrevented){

					if(sizes){
						if(isAuto){
							addClass(elem, lazySizesConfig.autosizesClass);
							autoSizer.updateElem(elem, true, width);
						} else {
							elem.setAttribute('sizes', sizes);
						}
					}

					srcset = elem.getAttribute(lazySizesConfig.srcsetAttr);
					src = elem.getAttribute(lazySizesConfig.srcAttr);

					if(isImg) {
						parent = elem.parentNode;
						isPicture = parent && regPicture.test(parent.nodeName || '');
					}

					firesLoad = event.detail.firesLoad || (('src' in elem) && (srcset || src || isPicture));

					event = {target: elem};

					if(firesLoad){
						addRemoveLoadEvents(elem, resetPreloading, true);
						clearTimeout(resetPreloadingTimer);
						resetPreloadingTimer = setTimeout(resetPreloading, 2500);

						addClass(elem, lazySizesConfig.loadingClass);
						addRemoveLoadEvents(elem, switchLoadingClass, true);
					}

					if(isPicture){
						forEach.call(parent.getElementsByTagName('source'), handleSources);
					}

					if(srcset){
						elem.setAttribute('srcset', srcset);
					} else if(src && !isPicture){
						if(regIframe.test(elem.nodeName)){
							changeIframeSrc(elem, src);
						} else {
							elem.src = src;
						}
					}

					if(srcset || isPicture){
						updatePolyfill(elem, {src: src});
					}
				}

				if( !firesLoad || elem.complete ){
					if(firesLoad){
						resetPreloading(event);
					} else {
						isLoading--;
					}
					switchLoadingClass(event);
				}
			});
		};

		var onload = function(){
			if(isCompleted){return;}
			if(Date.now() - started < 999){
				setTimeout(onload, 999);
				return;
			}
			var scrollTimer;
			var afterScroll = function(){
				lazySizesConfig.loadMode = 3;
				throttledCheckElements();
			};

			isCompleted = true;

			lazySizesConfig.loadMode = 3;

			if(!isLoading){
				throttledCheckElements();
			}

			addEventListener('scroll', function(){
				if(lazySizesConfig.loadMode == 3){
					lazySizesConfig.loadMode = 2;
				}
				clearTimeout(scrollTimer);
				scrollTimer = setTimeout(afterScroll, 99);
			}, true);
		};

		/*
		var onload = function(){
			var scrollTimer, timestamp;
			var wait = 99;
			var afterScroll = function(){
				var last = (Date.now()) - timestamp;

				// if the latest call was less that the wait period ago
				// then we reset the timeout to wait for the difference
				if (last < wait) {
					scrollTimer = setTimeout(afterScroll, wait - last);

					// or if not we can null out the timer and run the latest
				} else {
					scrollTimer = null;
					lazySizesConfig.loadMode = 3;
					throttledCheckElements();
				}
			};

			isCompleted = true;
			lowRuns += 8;

			lazySizesConfig.loadMode = 3;

			addEventListener('scroll', function(){
				timestamp = Date.now();
				if(!scrollTimer){
					lazySizesConfig.loadMode = 2;
					scrollTimer = setTimeout(afterScroll, wait);
				}
			}, true);
		};
		*/

		return {
			_: function(){
				started = Date.now();

				lazyloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass);
				preloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass + ' ' + lazySizesConfig.preloadClass);

				defaultExpand = lazySizesConfig.expand;
				preloadExpand = defaultExpand * lazySizesConfig.expFactor;

				addEventListener('scroll', throttledCheckElements, true);

				addEventListener('resize', throttledCheckElements, true);

				if(window.MutationObserver){
					new MutationObserver( throttledCheckElements ).observe( docElem, {childList: true, subtree: true, attributes: true} );
				} else {
					docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);
					docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);
					setInterval(throttledCheckElements, 999);
				}

				addEventListener('hashchange', throttledCheckElements, true);

				//, 'fullscreenchange'
				['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend', 'webkitAnimationEnd'].forEach(function(name){
					document[_addEventListener](name, throttledCheckElements, true);
				});

				if((/d$|^c/.test(document.readyState))){
					onload();
				} else {
					addEventListener('load', onload);
					document[_addEventListener]('DOMContentLoaded', throttledCheckElements);
					setTimeout(onload, 20000);
				}

				throttledCheckElements(lazyloadElems.length > 0);
			},
			checkElems: throttledCheckElements,
			unveil: unveilElement
		};
	})();


	var autoSizer = (function(){
		var autosizesElems;

		var sizeElement = function (elem, dataAttr, width){
			var sources, i, len, event;
			var parent = elem.parentNode;

			if(parent){
				width = getWidth(elem, parent, width);
				event = triggerEvent(elem, 'lazybeforesizes', {width: width, dataAttr: !!dataAttr});

				if(!event.defaultPrevented){
					width = event.detail.width;

					if(width && width !== elem._lazysizesWidth){
						elem._lazysizesWidth = width;
						width += 'px';
						elem.setAttribute('sizes', width);

						if(regPicture.test(parent.nodeName || '')){
							sources = parent.getElementsByTagName('source');
							for(i = 0, len = sources.length; i < len; i++){
								sources[i].setAttribute('sizes', width);
							}
						}

						if(!event.detail.dataAttr){
							updatePolyfill(elem, event.detail);
						}
					}
				}
			}
		};

		var updateElementsSizes = function(){
			var i;
			var len = autosizesElems.length;
			if(len){
				i = 0;

				for(; i < len; i++){
					sizeElement(autosizesElems[i]);
				}
			}
		};

		var throttledUpdateElementsSizes = throttle(updateElementsSizes);

		return {
			_: function(){
				autosizesElems = document.getElementsByClassName(lazySizesConfig.autosizesClass);
				addEventListener('resize', throttledUpdateElementsSizes);
			},
			checkElems: throttledUpdateElementsSizes,
			updateElem: sizeElement
		};
	})();

	var init = function(){
		if(!init.i){
			init.i = true;
			autoSizer._();
			loader._();
		}
	};

	(function(){
		var prop;

		var lazySizesDefaults = {
			lazyClass: 'lazyload',
			loadedClass: 'lazyloaded',
			loadingClass: 'lazyloading',
			preloadClass: 'lazypreload',
			errorClass: 'lazyerror',
			//strictClass: 'lazystrict',
			autosizesClass: 'lazyautosizes',
			srcAttr: 'data-src',
			srcsetAttr: 'data-srcset',
			sizesAttr: 'data-sizes',
			//preloadAfterLoad: false,
			minSize: 40,
			customMedia: {},
			init: true,
			expFactor: 1.7,
			expand: docElem.clientHeight > 630 ? docElem.clientWidth > 890 ? 500 : 410 : 359,
			loadMode: 2
		};

		lazySizesConfig = window.lazySizesConfig || window.lazysizesConfig || {};

		for(prop in lazySizesDefaults){
			if(!(prop in lazySizesConfig)){
				lazySizesConfig[prop] = lazySizesDefaults[prop];
			}
		}

		window.lazySizesConfig = lazySizesConfig;

		setTimeout(function(){
			if(lazySizesConfig.init){
				init();
			}
		});
	})();

	return {
		cfg: lazySizesConfig,
		autoSizer: autoSizer,
		loader: loader,
		init: init,
		uP: updatePolyfill,
		aC: addClass,
		rC: removeClass,
		hC: hasClass,
		fire: triggerEvent,
		gW: getWidth
	};
}));


// Codigo js para página Poema Panorámico

// @codekit-prepend "config-min.js"
// @codekit-prepend "librerias/lazysizes.js"


/*
	Variables
*/

var intro = document.querySelector(".js--intro");
var interactivo = document.querySelector(".js--interactivo");
// var contenido = document.querySelector(".js--contenido");
var el_audio = document.querySelector(".js--audio");
var area = document.querySelector(".js--mapa--area");
var ubic = document.querySelector(".js--mapa--ubic");
var mapa_poemas = document.querySelector(".js--mapa--poemas");
var indicacion = document.querySelector(".js--indicacion");
var prop = 0;
var poema = [];
var fondo = document.querySelector(".js--fondo");
var imgs_fondo = fondo.querySelectorAll("img");
var suma_alto = parseInt(0);
var suma_ancho = parseInt(0);
var drag = document.querySelector(".js--drag");
var alto_ventana = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var ancho_ventana = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var posY = 0;
var posX = 0;

var aceler_vert = 0;
var aceler_horiz = 0;

var texto__indicacion = document.querySelector(".js--texto__indicacion");

/*
	Funciones generales
*/

function btns_reproductor (estado_audio)
{
	var rep_flip = document.querySelector(".js-flip-play");

	if (estado_audio === "play" )
	{
		el_audio.play();
		rep_flip.classList.remove("cambio");
	}
	else if (estado_audio === "pausa" )
	{
		el_audio.pause();
		rep_flip.classList.add("cambio");
	}
}

function addEvent(obj, evt, fn)
{
	if (obj.addEventListener)
	{
		obj.addEventListener(evt, fn, false);
	}
	else if (obj.attachEvent)
	{
		obj.attachEvent("on" + evt, fn);
	}
}


/*
	Funciones específicas
*/

function audio_func ()
{
	var rep = document.querySelector(".js-reproducir");
	rep.addEventListener('click', function()
	{
		var estado_audio = this.getAttribute("data-estado");
		if ( estado_audio === "pausa" )
		{
			this.setAttribute("data-estado","play");
			estado_audio = "play";
		}
		else if (estado_audio === "play" )
		{
			this.setAttribute("data-estado","pausa");
			estado_audio = "pausa";
		}
		btns_reproductor(estado_audio);
	});
}

function altura_func ( mitad )
{
	// Falta controlar que no se mueva cuando llegue al tope
	if (posY < mitad)
	{
		if ( -drag.offsetTop > 0  )
		{
			drag.style.top = ( (drag.offsetTop + ( 10 *  aceler_vert ) ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( drag.offsetTop + ( 10 *  aceler_vert ) ) * prop * -1 ) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.top = 0 + "px";
			ubic.style.top = 0 + "px";
		}
	}

	if (posY > mitad)
	{
		if ( -drag.offsetTop < (suma_alto - alto_ventana) )
		{
			drag.style.top = ( ( drag.offsetTop - ( 10 *  aceler_vert ) ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( drag.offsetTop - ( 10 *  aceler_vert ) ) * prop * -1 ) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.top = ( ( ( suma_alto - alto_ventana ) * -1 ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( suma_alto - alto_ventana ) * prop ) ).toFixed(1) + "px";
		}
	}
}

function ancho_func ( mitad )
{
	if (posX < mitad )
	{
		if ( -drag.offsetLeft > 0 )
		{
			drag.style.left = ( (drag.offsetLeft + ( 10 *  aceler_horiz ) ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( drag.offsetLeft + ( 10 *  aceler_horiz ) ) * prop * -1) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.left = 0 + "px";
			ubic.style.left = 0 + "px";
		}
	}
	if (posX > mitad)
	{
		if ( -drag.offsetLeft < (suma_ancho - ancho_ventana) )
		{
			drag.style.left = ( (drag.offsetLeft - ( 10 *  aceler_horiz ) ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( drag.offsetLeft - ( 10 *  aceler_horiz ) ) * prop * -1) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.left = ( ( ( suma_ancho - ancho_ventana ) * -1 ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( suma_ancho - ancho_ventana ) * prop ) ).toFixed(1) + "px";
		}
	}
}

function mov_func()
{

	// Áreas sensibles (mitades de pantalla)
	var mitad_alto = ( alto_ventana - 16 ) / 2 ;
	var mitad_ancho = ( ancho_ventana - 16 ) / 2 ;

	// Ejecutar el movimiento cada 200 milisegundos
	var interval_mov = setInterval( function()
	{
		altura_func( mitad_alto );
		ancho_func( mitad_ancho );

		// Función cuando se mueve el mouse.
		// Identificar posición y asignarla al valor de aceleración vertical y horizontal
		var mie = (navigator.appName === "Microsoft Internet Explorer") ? true : false;

		document.addEventListener('mousemove', function(e)
		{
			// Hack IE
			if (!mie)
			{
				posY = e.pageY;
				posX = e.pageX; 
			}
			else
			{
				posY = event.clientY + document.body.scrollTop;
				posX = event.clientX + document.body.scrollLeft;
			}

			// Funciones de aceleración de acuerdo a la posición del mouse.
			function aceleracion_vert ()
			{
				if ( posY < mitad_alto )
				{
					aceler_vert = ( ( ( mitad_alto - posY ) * 100 ) / mitad_alto ) * 0.07;
				}
				else if ( posY > mitad_alto )
				{
					aceler_vert = ( ( ( posY - mitad_alto ) * 100 ) / ( mitad_alto * 1 ) ) * 0.07;
				}
			}
			function aceleracion_horz ()
			{
				if ( posX < mitad_ancho )
				{
					aceler_horiz = ( ( ( mitad_ancho - posX ) * 100 ) / mitad_ancho ) * 0.07;
				}
				else if ( posX > mitad_ancho )
				{
					aceler_horiz = ( ( ( posX - mitad_ancho ) * 100 ) / ( mitad_ancho * 1 ) ) * 0.07;
				}
			}

			aceleracion_vert();
			aceleracion_horz();

			
			// var pos_derecha = (suma_ancho - ancho_ventana ) * posX / ancho_ventana;
			// var pos_abajo = (suma_alto - alto_ventana ) * posY / alto_ventana;
		});
	}, 100);

	//  Detectar si el mouse está o no en la pantalla
	addEvent(document, 'mouseout', function()
	{
		aceler_vert = 0;
		aceler_horiz = 0;
	});
}

function generales ()
{
	document.title = document.querySelector("h1").innerHTML;
	texto__indicacion.innerHTML = "Mueve el mouse.";
}

function posicion_inicial() {
	// Posiciono el drag hasta abajo a la derehca
	drag.style.top = ( (suma_alto - alto_ventana ) * -1 ) + "px";
	ubic.style.top = ( ( (suma_alto - alto_ventana ) ) * prop) + "px";
	var left_inicial = ( (suma_ancho - ancho_ventana ) * -1 ) + "px";
	drag.style.left = left_inicial;
	ubic.style.left = ( ( (suma_ancho - ancho_ventana ) ) * prop) + "px";
}

function mapa_func (area_alto, area_ancho, ubic_alto, ubic_ancho) {
	area_alto = area_alto * prop;
	area_ancho = area_ancho * prop;
	ubic_alto = ubic_alto * prop;
	ubic_ancho = ubic_ancho * prop;
	
	area.style.height = area_alto + 'px';
	area.style.width = area_ancho + 'px';

	ubic.style.height = ubic_alto + 'px';
	ubic.style.width = ubic_ancho + 'px';

	posicion_inicial();
}

function fondo_func ()
{

	var poemas_canvas = document.querySelector(".js--poemas");

	var imgs_dimensiones = [];

	var num_filas = parseInt(fondo.getAttribute("data-filas"));
	var num_imgs = parseInt(fondo.getAttribute("data-imagenes"));

	var columns = num_imgs / num_filas;

	for( var i=0, j=num_filas; i<j; i++)
	{
		// Obtengo la primer imagen de cada columna
		var primer_img = imgs_fondo[ i * columns ];

		// Obtengo sus medidas
		var alto_fila = parseInt(primer_img.getAttribute("data-height"));
		var ancho_fila = parseInt(primer_img.getAttribute("data-width")) * columns;
		// Junto las medidas en un array
		imgs_dimensiones.push( [ alto_fila, ancho_fila ] );
	}

	var dims_medidas = imgs_dimensiones.length;

	suma_alto = parseInt(0);
	suma_ancho = parseInt(0);
	var suma_ancho_arr = [];

	for( var k=0, l=dims_medidas; k<l; k++)
	{
		suma_alto = suma_alto + imgs_dimensiones[ k ][0];
		suma_ancho_arr.push( imgs_dimensiones[ k ][1] );
	}
	suma_ancho = Math.max( parseInt(suma_ancho_arr) );

	// le doy las medidas de la suma de las imagenes a .js--fondo
	fondo.style.height = suma_alto + 'px';
	fondo.style.width = suma_ancho + 'px';
	poemas_canvas.style.height = suma_alto + 'px';
	poemas_canvas.style.width = suma_ancho + 'px';
	drag.style.height = suma_alto + 'px';
	drag.style.width = suma_ancho + 'px';

	mapa_func(suma_alto, suma_ancho, alto_ventana, ancho_ventana);
}

function poemas_func ()
{

	function Poema(poe, right, bottom, i)
	{
		this.div = poe;
		this.indx = i;
		this.derecha = right;
		this.abajo = bottom;

		this.posicion = function()
		{
			this.div.style.right = this.derecha + 'px';
			this.div.style.bottom = this.abajo + 'px';

			mapa_poemas.insertAdjacentHTML('afterbegin', "<div class='mapa--punto' style='right:"+ ( this.derecha * prop ) +"px; bottom:"+ ( this.abajo * prop ) +"px;'></div>");
		};

		this.mostrar = function()
		{
			this.punto = this.div.querySelector(".js--poema--punto");
			this.contenido = this.div.querySelector(".js--poema--content");

			this.punto.addEventListener('mouseover', function()
			{
				poema[i].contenido.classList.add("visible");
				aceler_vert = 0;
				aceler_horiz = 0;
			});
			this.punto.addEventListener('mouseout', function()
			{
				poema[i].contenido.classList.remove("visible");
			});
		};

	}

	var los_poemas = document.querySelectorAll(".js--poema");

	Array.prototype.forEach.call(los_poemas, function(poe, i)
	{
		// var num = poe.getAttribute("data-poema");
		var right = poe.getAttribute("data-right");
		var bottom = poe.getAttribute("data-bottom");

		poema[i] = new Poema(poe, right, bottom, i);
		poema[i].posicion();
		poema[i].mostrar();
	});
}

function entrada() {

	var intro_btn = document.querySelector(".js--intro--btn");
	if (intro_btn) {
		intro_btn.addEventListener('click', function() {
			intro.classList.add("oculto");
			interactivo.classList.add("visible");

			setTimeout(function(){
				intro.classList.add("atras");
			}, 1000);

			// al terminar la animación de la indicacion
			indicacion.classList.add("visible");

			setTimeout(function(){
				indicacion.classList.add("atras");
			}, 2000);

			// Funciones ejecutadas hasta entrar
			el_audio.play();
			audio_func();
			poemas_func();
			mov_func();
		});
	}
}

function salida()
{
	var salida_btn = document.querySelector(".js--regresar--btn");
	if (salida_btn)
	{
		salida_btn.addEventListener('click', function()
		{
			intro.classList.remove("atras");
			intro.classList.remove("oculto");
			interactivo.classList.remove("visible");
			el_audio.pause();
			posicion_inicial();
		});
	}
}

function responsive ()
{
	if ( ancho_ventana > 1401 )
	{
		prop = 0.03;
	}
	if (ancho_ventana < 1400 && ancho_ventana > 1201)
	{
		prop = 0.023;
	}
	else if (ancho_ventana < 1200 && ancho_ventana > 801)
	{
		prop = 0.012;
	}
	else if (ancho_ventana < 800 && ancho_ventana > 651)
	{
		prop = 0.009;
	}
	else if (ancho_ventana < 650 && ancho_ventana > 401)
	{
		prop = 0.03;

		var poemas = document.querySelectorAll('.js--poema');
		console.log(poemas);

		Array.prototype.forEach.call(poemas, function(poe) {
			var right = poe.getAttribute("data-right");
			var nuevo_right = parseInt(right) / 2;
			poe.setAttribute("data-right",  nuevo_right);

			var bottom = poe.getAttribute("data-bottom");
			var nuevo_bottom = parseInt(bottom) / 2;
			poe.setAttribute("data-bottom",  nuevo_bottom);
		});
		
		var fondos = document.querySelectorAll('.js--fondo--img');

		Array.prototype.forEach.call(fondos, function(el) {
			var data_height = el.getAttribute("data-height");
			var nuevo_data_height = parseInt(data_height) / 2;
			el.setAttribute("data-height",  nuevo_data_height);

			var data_width = el.getAttribute("data-width");
			var nuevo_data_width = parseInt(data_width) / 2;
			el.setAttribute("data-width",  nuevo_data_width);

			var dat_height = el.getAttribute("height");
			var nuevo_height = parseInt(dat_height) / 2;
			el.setAttribute("height",  nuevo_height );

			var dat_width = el.getAttribute("width");
			var nuevo_width = parseInt(dat_width) / 2;
			el.setAttribute("width",  nuevo_width );
		});
	}
	else if ( ancho_ventana < 400)
	{
		prop = 0.023;
		console.log("responsiveamos");
		var poemas_div = document.querySelectorAll('.js--poema');

		Array.prototype.forEach.call(poemas_div, function(poe) {
			var right = poe.getAttribute("data-right");
			var nuevo_right = parseInt(right) / 2.5;
			poe.setAttribute("data-right",  nuevo_right);

			var bottom = poe.getAttribute("data-bottom");
			var nuevo_bottom = parseInt(bottom) / 2.5;
			poe.setAttribute("data-bottom",  nuevo_bottom);
		});
		
		var fondos_div = document.querySelectorAll('.js--fondo--img');

		Array.prototype.forEach.call(fondos_div, function(el) {
			var data_height = el.getAttribute("data-height");
			var nuevo_data_height = parseInt(data_height) / 2.5;
			el.setAttribute("data-height",  nuevo_data_height);

			var data_width = el.getAttribute("data-width");
			var nuevo_data_width = parseInt(data_width) / 2.5;
			el.setAttribute("data-width",  nuevo_data_width);

			var dat_height = el.getAttribute("height");
			var nuevo_height = parseInt(dat_height) / 2.5;
			el.setAttribute("height",  nuevo_height );

			var dat_width = el.getAttribute("width");
			var nuevo_width = parseInt(dat_width) / 2.5;
			el.setAttribute("width",  nuevo_width );
		});
	}
}

document.addEventListener('DOMContentLoaded', function()
{
	responsive();
	generales();
	entrada();
	salida();
	fondo_func();
});

