// Codigo js para página Poema Panorámico

// @codekit-prepend "config-min.js"
// @codekit-prepend "librerias/jquery.js"
// @codekit-prepend "librerias/jquery-ui.min.js"
// @codekit-prepend "librerias/jquery.ui.touch-punch.min.js"
// @codekit-prepend "librerias/lazysizes.js"


/*
	Variables
*/

var intro = document.querySelector(".js--intro");
var interactivo = document.querySelector(".js--interactivo");
var contenido = document.querySelector(".js--contenido");
var el_audio = document.querySelector(".js--audio");
var area = document.querySelector(".js--mapa--area");
var ubic = document.querySelector(".js--mapa--ubic");
var mapa_poemas = document.querySelector(".js--mapa--poemas");
var indicacion = document.querySelector(".js--indicacion");
var prop = 0.012;
var poema = [];
var fondo = document.querySelector(".js--fondo");
var imgs_fondo = fondo.querySelectorAll("img");

var texto__indicacion = document.querySelector(".js--texto__indicacion");

/*
	Funciones generales
*/

function btns_reproductor (estado_audio)
{
	var rep_flip = document.querySelector(".js-flip-play");

	if (estado_audio === "play" )
	{
		el_audio.play();
		rep_flip.classList.remove("cambio");
	}
	else if (estado_audio === "pausa" )
	{
		el_audio.pause();
		rep_flip.classList.add("cambio");
	}
}

/*
	Funciones específicas
*/

function audio_func ()
{
	var rep = document.querySelector(".js-reproducir");
	rep.addEventListener('click', function()
	{
		var estado_audio = this.getAttribute("data-estado");
		if ( estado_audio === "pausa" )
		{
			this.setAttribute("data-estado","play");
			estado_audio = "play";
		}
		else if (estado_audio === "play" )
		{
			this.setAttribute("data-estado","pausa");
			estado_audio = "pausa";
		}
		btns_reproductor(estado_audio);
	});
}

function entrada()
{
	var intro_btn = document.querySelector(".js--intro--btn");
	if (intro_btn)
	{
		intro_btn.addEventListener('click', function()
		{
			intro.classList.add("oculto");
			interactivo.classList.add("visible");

			setTimeout(function(){
				intro.classList.add("atras");
			}, 1000);

			// al terminar la animación de la indicacion
			indicacion.classList.add("visible");

			setTimeout(function(){
				indicacion.classList.add("atras");
				indicacion.classList.add("oculto");
			}, 2000);

			el_audio.play();
		});
	}
}

function salida()
{
	var salida_btn = document.querySelector(".js--regresar--btn");
	if (salida_btn)
	{
		salida_btn.addEventListener('click', function()
		{
			intro.classList.remove("atras");
			intro.classList.remove("oculto");
			interactivo.classList.remove("visible");
			el_audio.pause();
		});
	}
}

function drag_func()
{
	$( ".js--drag" ).draggable(
	{
		containment: ".js--contenido",
		scroll: false,
		drag: function( event, ui )
		{
			ubic.style.right = ui.position.left * prop + "px";
			ubic.style.bottom = ui.position.top * prop + "px";
		}
	});
}

function generales ()
{
	document.title = document.querySelector("h1").innerHTML;
	texto__indicacion.innerHTML = "Arrastra la imagen.";
}

function mapa_func (area_alto, area_ancho, ubic_alto, ubic_ancho) {
	area_alto = area_alto * prop;
	area_ancho = area_ancho * prop;
	ubic_alto = ubic_alto * prop;
	ubic_ancho = ubic_ancho * prop;
	
	area.style.height = area_alto + 'px';
	area.style.width = area_ancho + 'px';

	ubic.style.height = ubic_alto + 'px';
	ubic.style.width = ubic_ancho + 'px';
}

function fondo_func ()
{
	var ancho_ventana = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	var alto_ventana = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

	var drag = document.querySelector(".js--drag");
	var poemas_canvas = document.querySelector(".js--poemas");

	var imgs_dimensiones = [];

	var num_filas = parseInt(fondo.getAttribute("data-filas"));
	var num_imgs = parseInt(fondo.getAttribute("data-imagenes"));

	var columns = num_imgs / num_filas;

	for( var i=0, j=num_filas; i<j; i++)
	{
		// Obtengo la primer imagen de cada columna
		var primer_img = imgs_fondo[ i * columns ];

		// Obtengo sus medidas
		var alto_fila = parseInt(primer_img.getAttribute("data-height"));
		var ancho_fila = parseInt(primer_img.getAttribute("data-width")) * columns;
		// Junto las medidas en un array
		imgs_dimensiones.push( [ alto_fila, ancho_fila ] );
	}

	var dims_medidas = imgs_dimensiones.length;

	var suma_alto = parseInt(0);
	var suma_ancho = parseInt(0);
	var suma_ancho_arr = [];

	for( var k=0, l=dims_medidas; k<l; k++)
	{
		suma_alto = suma_alto + imgs_dimensiones[ k ][0];
		suma_ancho_arr.push( imgs_dimensiones[ k ][1] );
	}
	suma_ancho = Math.max( parseInt(suma_ancho_arr) );

	// le doy las medidas de la suma de las imagenes a .js--fondo
	fondo.style.height = suma_alto + 'px';
	fondo.style.width = suma_ancho + 'px';
	poemas_canvas.style.height = suma_alto + 'px';
	poemas_canvas.style.width = suma_ancho + 'px';
	drag.style.height = suma_alto + 'px';
	drag.style.width = suma_ancho + 'px';
	contenido.style.height = ( suma_alto * 2 - ( alto_ventana) ) + 'px';
	contenido.style.width = ( suma_ancho * 2 - ( ancho_ventana) ) + 'px';

	mapa_func(suma_alto, suma_ancho, alto_ventana, ancho_ventana);
}

function poemas_func ()
{

	function Poema(poe, right, bottom, i)
	{
		this.div = poe;
		this.indx = i;
		this.derecha = right;
		this.abajo = bottom;

		this.posicion = function()
		{
			this.div.style.right = this.derecha + 'px';
			this.div.style.bottom = this.abajo + 'px';

			mapa_poemas.insertAdjacentHTML('afterbegin', "<div class='mapa--punto' style='right:"+ ( this.derecha * prop ) +"px; bottom:"+ ( this.abajo * prop ) +"px;'></div>");
		};

		this.mostrar = function()
		{
			this.punto = this.div.querySelector(".js--poema--punto");
			this.contenido = this.div.querySelector(".js--poema--content");

			this.punto.addEventListener('click', function()
			{
				poema[i].contenido.classList.toggle("visible");
			});
		};

	}

	var los_poemas = document.querySelectorAll(".js--poema");

	Array.prototype.forEach.call(los_poemas, function(poe, i)
	{
		// var num = poe.getAttribute("data-poema");
		var right = poe.getAttribute("data-right");
		var bottom = poe.getAttribute("data-bottom");

		poema[i] = new Poema(poe, right, bottom, i);
		poema[i].posicion();
		poema[i].mostrar();
	});
}
function responsive ()
{
	if ( screen.width <= 650 ) {
		console.log("responsiveamos");
		var poemas = document.querySelectorAll('.js--poema');
		console.log(poemas);

		Array.prototype.forEach.call(poemas, function(poe) {
			var right = poe.getAttribute("data-right");
			var nuevo_right = parseInt(right) / 3;
			poe.setAttribute("data-right",  nuevo_right);

			var bottom = poe.getAttribute("data-bottom");
			var nuevo_bottom = parseInt(bottom) / 3;
			poe.setAttribute("data-bottom",  nuevo_bottom);
		});
		
		var fondos = document.querySelectorAll('.js--fondo--img');

		Array.prototype.forEach.call(fondos, function(el) {
			var data_height = el.getAttribute("data-height");
			var nuevo_data_height = parseInt(data_height) / 3;
			el.setAttribute("data-height",  nuevo_data_height);

			var data_width = el.getAttribute("data-width");
			var nuevo_data_width = parseInt(data_width) / 3;
			el.setAttribute("data-width",  nuevo_data_width);

			var dat_height = el.getAttribute("height");
			var nuevo_height = parseInt(dat_height) / 3;
			el.setAttribute("height",  nuevo_height );

			var dat_width = el.getAttribute("width");
			var nuevo_width = parseInt(dat_width) / 3;
			el.setAttribute("width",  nuevo_width );
		});
	}

	if ( ancho_ventana > 1401 )
	{
		prop = 0.03;
	}
	if (ancho_ventana < 1400 && ancho_ventana > 1201)
	{
		prop = 0.023;
	}
	else if (ancho_ventana < 1200 && ancho_ventana > 801)
	{
		prop = 0.012;
	}
	else if (ancho_ventana < 800 && ancho_ventana > 651)
	{
		prop = 0.009;
	}
	else if (ancho_ventana < 650 && ancho_ventana > 401)
	{
		prop = 0.03;
	}
	else if ( ancho_ventana < 400)
	{
		prop = 0.023;
	}
}


document.addEventListener('DOMContentLoaded', function()
{
	responsive();
	generales();
	entrada();
	salida();
	fondo_func();
	drag_func();
	audio_func();
	poemas_func();
});