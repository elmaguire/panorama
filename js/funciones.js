// Codigo js para página Poema Panorámico

// @codekit-prepend "config-min.js"
// @codekit-prepend "librerias/lazysizes.js"


/*
	Variables
*/

var intro = document.querySelector(".js--intro");
var interactivo = document.querySelector(".js--interactivo");
// var contenido = document.querySelector(".js--contenido");
var el_audio = document.querySelector(".js--audio");
var area = document.querySelector(".js--mapa--area");
var ubic = document.querySelector(".js--mapa--ubic");
var mapa_poemas = document.querySelector(".js--mapa--poemas");
var indicacion = document.querySelector(".js--indicacion");
var prop = 0;
var poema = [];
var fondo = document.querySelector(".js--fondo");
var imgs_fondo = fondo.querySelectorAll("img");
var suma_alto = parseInt(0);
var suma_ancho = parseInt(0);
var drag = document.querySelector(".js--drag");
var alto_ventana = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var ancho_ventana = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var posY = 0;
var posX = 0;

var aceler_vert = 0;
var aceler_horiz = 0;

var texto__indicacion = document.querySelector(".js--texto__indicacion");

/*
	Funciones generales
*/

function btns_reproductor (estado_audio)
{
	var rep_flip = document.querySelector(".js-flip-play");

	if (estado_audio === "play" )
	{
		el_audio.play();
		rep_flip.classList.remove("cambio");
	}
	else if (estado_audio === "pausa" )
	{
		el_audio.pause();
		rep_flip.classList.add("cambio");
	}
}

function addEvent(obj, evt, fn)
{
	if (obj.addEventListener)
	{
		obj.addEventListener(evt, fn, false);
	}
	else if (obj.attachEvent)
	{
		obj.attachEvent("on" + evt, fn);
	}
}


/*
	Funciones específicas
*/

function audio_func ()
{
	var rep = document.querySelector(".js-reproducir");
	rep.addEventListener('click', function()
	{
		var estado_audio = this.getAttribute("data-estado");
		if ( estado_audio === "pausa" )
		{
			this.setAttribute("data-estado","play");
			estado_audio = "play";
		}
		else if (estado_audio === "play" )
		{
			this.setAttribute("data-estado","pausa");
			estado_audio = "pausa";
		}
		btns_reproductor(estado_audio);
	});
}

function altura_func ( mitad )
{
	// Falta controlar que no se mueva cuando llegue al tope
	if (posY < mitad)
	{
		if ( -drag.offsetTop > 0  )
		{
			drag.style.top = ( (drag.offsetTop + ( 10 *  aceler_vert ) ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( drag.offsetTop + ( 10 *  aceler_vert ) ) * prop * -1 ) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.top = 0 + "px";
			ubic.style.top = 0 + "px";
		}
	}

	if (posY > mitad)
	{
		if ( -drag.offsetTop < (suma_alto - alto_ventana) )
		{
			drag.style.top = ( ( drag.offsetTop - ( 10 *  aceler_vert ) ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( drag.offsetTop - ( 10 *  aceler_vert ) ) * prop * -1 ) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.top = ( ( ( suma_alto - alto_ventana ) * -1 ) ).toFixed(1) + "px";
			ubic.style.top = ( ( ( suma_alto - alto_ventana ) * prop ) ).toFixed(1) + "px";
		}
	}
}

function ancho_func ( mitad )
{
	if (posX < mitad )
	{
		if ( -drag.offsetLeft > 0 )
		{
			drag.style.left = ( (drag.offsetLeft + ( 10 *  aceler_horiz ) ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( drag.offsetLeft + ( 10 *  aceler_horiz ) ) * prop * -1) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.left = 0 + "px";
			ubic.style.left = 0 + "px";
		}
	}
	if (posX > mitad)
	{
		if ( -drag.offsetLeft < (suma_ancho - ancho_ventana) )
		{
			drag.style.left = ( (drag.offsetLeft - ( 10 *  aceler_horiz ) ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( drag.offsetLeft - ( 10 *  aceler_horiz ) ) * prop * -1) ).toFixed(1) + "px";
		}
		else
		{
			drag.style.left = ( ( ( suma_ancho - ancho_ventana ) * -1 ) ).toFixed(1) + "px";
			ubic.style.left = ( ( ( suma_ancho - ancho_ventana ) * prop ) ).toFixed(1) + "px";
		}
	}
}

function mov_func()
{

	// Áreas sensibles (mitades de pantalla)
	var mitad_alto = ( alto_ventana - 16 ) / 2 ;
	var mitad_ancho = ( ancho_ventana - 16 ) / 2 ;

	// Ejecutar el movimiento cada 200 milisegundos
	var interval_mov = setInterval( function()
	{
		altura_func( mitad_alto );
		ancho_func( mitad_ancho );

		// Función cuando se mueve el mouse.
		// Identificar posición y asignarla al valor de aceleración vertical y horizontal
		var mie = (navigator.appName === "Microsoft Internet Explorer") ? true : false;

		document.addEventListener('mousemove', function(e)
		{
			// Hack IE
			if (!mie)
			{
				posY = e.pageY;
				posX = e.pageX; 
			}
			else
			{
				posY = event.clientY + document.body.scrollTop;
				posX = event.clientX + document.body.scrollLeft;
			}

			// Funciones de aceleración de acuerdo a la posición del mouse.
			function aceleracion_vert ()
			{
				if ( posY < mitad_alto )
				{
					aceler_vert = ( ( ( mitad_alto - posY ) * 100 ) / mitad_alto ) * 0.07;
				}
				else if ( posY > mitad_alto )
				{
					aceler_vert = ( ( ( posY - mitad_alto ) * 100 ) / ( mitad_alto * 1 ) ) * 0.07;
				}
			}
			function aceleracion_horz ()
			{
				if ( posX < mitad_ancho )
				{
					aceler_horiz = ( ( ( mitad_ancho - posX ) * 100 ) / mitad_ancho ) * 0.07;
				}
				else if ( posX > mitad_ancho )
				{
					aceler_horiz = ( ( ( posX - mitad_ancho ) * 100 ) / ( mitad_ancho * 1 ) ) * 0.07;
				}
			}

			aceleracion_vert();
			aceleracion_horz();

			
			// var pos_derecha = (suma_ancho - ancho_ventana ) * posX / ancho_ventana;
			// var pos_abajo = (suma_alto - alto_ventana ) * posY / alto_ventana;
		});
	}, 100);

	//  Detectar si el mouse está o no en la pantalla
	addEvent(document, 'mouseout', function()
	{
		aceler_vert = 0;
		aceler_horiz = 0;
	});
}

function generales ()
{
	document.title = document.querySelector("h1").innerHTML;
	texto__indicacion.innerHTML = "Mueve el mouse.";
}

function posicion_inicial() {
	// Posiciono el drag hasta abajo a la derehca
	drag.style.top = ( (suma_alto - alto_ventana ) * -1 ) + "px";
	ubic.style.top = ( ( (suma_alto - alto_ventana ) ) * prop) + "px";
	var left_inicial = ( (suma_ancho - ancho_ventana ) * -1 ) + "px";
	drag.style.left = left_inicial;
	ubic.style.left = ( ( (suma_ancho - ancho_ventana ) ) * prop) + "px";
}

function mapa_func (area_alto, area_ancho, ubic_alto, ubic_ancho) {
	area_alto = area_alto * prop;
	area_ancho = area_ancho * prop;
	ubic_alto = ubic_alto * prop;
	ubic_ancho = ubic_ancho * prop;
	
	area.style.height = area_alto + 'px';
	area.style.width = area_ancho + 'px';

	ubic.style.height = ubic_alto + 'px';
	ubic.style.width = ubic_ancho + 'px';

	posicion_inicial();
}

function fondo_func ()
{

	var poemas_canvas = document.querySelector(".js--poemas");

	var imgs_dimensiones = [];

	var num_filas = parseInt(fondo.getAttribute("data-filas"));
	var num_imgs = parseInt(fondo.getAttribute("data-imagenes"));

	var columns = num_imgs / num_filas;

	for( var i=0, j=num_filas; i<j; i++)
	{
		// Obtengo la primer imagen de cada columna
		var primer_img = imgs_fondo[ i * columns ];

		// Obtengo sus medidas
		var alto_fila = parseInt(primer_img.getAttribute("data-height"));
		var ancho_fila = parseInt(primer_img.getAttribute("data-width")) * columns;
		// Junto las medidas en un array
		imgs_dimensiones.push( [ alto_fila, ancho_fila ] );
	}

	var dims_medidas = imgs_dimensiones.length;

	suma_alto = parseInt(0);
	suma_ancho = parseInt(0);
	var suma_ancho_arr = [];

	for( var k=0, l=dims_medidas; k<l; k++)
	{
		suma_alto = suma_alto + imgs_dimensiones[ k ][0];
		suma_ancho_arr.push( imgs_dimensiones[ k ][1] );
	}
	suma_ancho = Math.max( parseInt(suma_ancho_arr) );

	// le doy las medidas de la suma de las imagenes a .js--fondo
	fondo.style.height = suma_alto + 'px';
	fondo.style.width = suma_ancho + 'px';
	poemas_canvas.style.height = suma_alto + 'px';
	poemas_canvas.style.width = suma_ancho + 'px';
	drag.style.height = suma_alto + 'px';
	drag.style.width = suma_ancho + 'px';

	mapa_func(suma_alto, suma_ancho, alto_ventana, ancho_ventana);
}

function poemas_func ()
{

	function Poema(poe, right, bottom, i)
	{
		this.div = poe;
		this.indx = i;
		this.derecha = right;
		this.abajo = bottom;

		this.posicion = function()
		{
			this.div.style.right = this.derecha + 'px';
			this.div.style.bottom = this.abajo + 'px';

			mapa_poemas.insertAdjacentHTML('afterbegin', "<div class='mapa--punto' style='right:"+ ( this.derecha * prop ) +"px; bottom:"+ ( this.abajo * prop ) +"px;'></div>");
		};

		this.mostrar = function()
		{
			this.punto = this.div.querySelector(".js--poema--punto");
			this.contenido = this.div.querySelector(".js--poema--content");

			this.punto.addEventListener('mouseover', function()
			{
				poema[i].contenido.classList.add("visible");
				aceler_vert = 0;
				aceler_horiz = 0;
			});
			this.punto.addEventListener('mouseout', function()
			{
				poema[i].contenido.classList.remove("visible");
			});
		};

	}

	var los_poemas = document.querySelectorAll(".js--poema");

	Array.prototype.forEach.call(los_poemas, function(poe, i)
	{
		// var num = poe.getAttribute("data-poema");
		var right = poe.getAttribute("data-right");
		var bottom = poe.getAttribute("data-bottom");

		poema[i] = new Poema(poe, right, bottom, i);
		poema[i].posicion();
		poema[i].mostrar();
	});
}

function entrada() {

	var intro_btn = document.querySelector(".js--intro--btn");
	if (intro_btn) {
		intro_btn.addEventListener('click', function() {
			intro.classList.add("oculto");
			interactivo.classList.add("visible");

			setTimeout(function(){
				intro.classList.add("atras");
			}, 1000);

			// al terminar la animación de la indicacion
			indicacion.classList.add("visible");

			setTimeout(function(){
				indicacion.classList.add("atras");
			}, 2000);

			// Funciones ejecutadas hasta entrar
			el_audio.play();
			audio_func();
			poemas_func();
			mov_func();
		});
	}
}

function salida()
{
	var salida_btn = document.querySelector(".js--regresar--btn");
	if (salida_btn)
	{
		salida_btn.addEventListener('click', function()
		{
			intro.classList.remove("atras");
			intro.classList.remove("oculto");
			interactivo.classList.remove("visible");
			el_audio.pause();
			posicion_inicial();
		});
	}
}

function responsive ()
{
	if ( ancho_ventana > 1401 )
	{
		prop = 0.03;
	}
	if (ancho_ventana < 1400 && ancho_ventana > 1201)
	{
		prop = 0.023;
	}
	else if (ancho_ventana < 1200 && ancho_ventana > 801)
	{
		prop = 0.012;
	}
	else if (ancho_ventana < 800 && ancho_ventana > 651)
	{
		prop = 0.009;
	}
	else if (ancho_ventana < 650 && ancho_ventana > 401)
	{
		prop = 0.03;

		var poemas = document.querySelectorAll('.js--poema');
		console.log(poemas);

		Array.prototype.forEach.call(poemas, function(poe) {
			var right = poe.getAttribute("data-right");
			var nuevo_right = parseInt(right) / 2;
			poe.setAttribute("data-right",  nuevo_right);

			var bottom = poe.getAttribute("data-bottom");
			var nuevo_bottom = parseInt(bottom) / 2;
			poe.setAttribute("data-bottom",  nuevo_bottom);
		});
		
		var fondos = document.querySelectorAll('.js--fondo--img');

		Array.prototype.forEach.call(fondos, function(el) {
			var data_height = el.getAttribute("data-height");
			var nuevo_data_height = parseInt(data_height) / 2;
			el.setAttribute("data-height",  nuevo_data_height);

			var data_width = el.getAttribute("data-width");
			var nuevo_data_width = parseInt(data_width) / 2;
			el.setAttribute("data-width",  nuevo_data_width);

			var dat_height = el.getAttribute("height");
			var nuevo_height = parseInt(dat_height) / 2;
			el.setAttribute("height",  nuevo_height );

			var dat_width = el.getAttribute("width");
			var nuevo_width = parseInt(dat_width) / 2;
			el.setAttribute("width",  nuevo_width );
		});
	}
	else if ( ancho_ventana < 400)
	{
		prop = 0.023;
		console.log("responsiveamos");
		var poemas_div = document.querySelectorAll('.js--poema');

		Array.prototype.forEach.call(poemas_div, function(poe) {
			var right = poe.getAttribute("data-right");
			var nuevo_right = parseInt(right) / 2.5;
			poe.setAttribute("data-right",  nuevo_right);

			var bottom = poe.getAttribute("data-bottom");
			var nuevo_bottom = parseInt(bottom) / 2.5;
			poe.setAttribute("data-bottom",  nuevo_bottom);
		});
		
		var fondos_div = document.querySelectorAll('.js--fondo--img');

		Array.prototype.forEach.call(fondos_div, function(el) {
			var data_height = el.getAttribute("data-height");
			var nuevo_data_height = parseInt(data_height) / 2.5;
			el.setAttribute("data-height",  nuevo_data_height);

			var data_width = el.getAttribute("data-width");
			var nuevo_data_width = parseInt(data_width) / 2.5;
			el.setAttribute("data-width",  nuevo_data_width);

			var dat_height = el.getAttribute("height");
			var nuevo_height = parseInt(dat_height) / 2.5;
			el.setAttribute("height",  nuevo_height );

			var dat_width = el.getAttribute("width");
			var nuevo_width = parseInt(dat_width) / 2.5;
			el.setAttribute("width",  nuevo_width );
		});
	}
}

document.addEventListener('DOMContentLoaded', function()
{
	responsive();
	generales();
	entrada();
	salida();
	fondo_func();
});