/*

Documento de los contenidos del interactivo panorámico.

*/

var contenidoInteractivo =
{
	titulo : "POEMA PANORAMA", // Título de la página en general
	url : "http://", // URL de la página web
	descripcion : "", // Descripción de la página web
	ruta_imagen_home : "assets/imgs/bg_cielo.jpg", // Ruta de imagen del home
	ruta_audio : "assets/audios/dfpanoramico.mp3", // Ruta del audio del interactivo. Debe ser un archivo MP3
	footer : "<p>snd/imgn/txt: pablo martínez zárate<br>prgm/web: gerardo vidal</p><p><span class='naranja'>*</span> la plataforma tiene una versión en instalación interactiva, visita la página del artista (<a href='http://pablomz.info' target='_blank'>pablomz.info</a>) para información sobre activaciones y documentación del proyecto.</p>", // Texto que aparece en la página principal del interactivo. Admite etiquetas HTML y debe comenzar con la etiqueta <p> y terminar con</p>
	fondo: {
		filas : "3",  // Total de filas de imágenes del fondo
		imagenes : "60" // Total de segmentos de imágenes del fondo
	},
	fondos: [
		/*
		*/
		{
			// Datos de las imágenes del fondo
			col: "1", // Columna en la que aparece
			alto: "400", // Medida en pixeles de la imagen
			ancho: "1404", // Medida en pixeles de la imagen
			ruta_imagen: "assets/imgs/img_cielo_001.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_002.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_003.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_004.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_005.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_006.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_007.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_008.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_009.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_010.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_011.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_012.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_013.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_014.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_015.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_016.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_017.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_018.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_019.jpg"
		},
		{
			col: "1",
			alto: "400",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_020.jpg"
		},
		{
			// Datos de las imágenes del fondo
			col: "2", // Columna en la que aparece
			alto: "582", // Medida en pixeles de la imagen
			ancho: "1404", // Medida en pixeles de la imagen
			ruta_imagen: "assets/imgs/img_cielo_01.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_02.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_03.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_04.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_05.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_06.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_07.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_08.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_09.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_10.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_12.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_13.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_14.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_15.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_16.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_17.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_18.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_19.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_20.jpg"
		},
		{
			col: "2",
			alto: "582",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_21.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_23.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_24.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_25.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_26.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_27.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_28.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_29.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_30.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_31.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_32.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_33.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_34.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_35.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_36.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_37.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_38.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_39.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_40.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_41.jpg"
		},
		{
			col: "3",
			alto: "618",
			ancho: "1404",
			ruta_imagen: "assets/imgs/img_cielo_42.jpg"
		}
	],
	poemas: [
		/*
		*/
		{
			// Datos del poema
			derecha: "830",
			abajo: "690",
			texto: "<p>De lejos parece la enfermedad<br>corriéndose sobre la piel del mundo.<br>Podría decir que éste es el fin o<br>el comienzo de la ciudad,<br>pero sería mentira.</p>"
		},
		{
			// Datos del poema
			derecha: "1984",
			abajo: "924",
			texto: "<p>La luz retoca los relieves.<br>“Iré pronto”, me engaño. Ruidosa<br>la civilización a la distancia.<br>La sombra de un águila corta<br>mi sueño de tajo. De vuelta al autobús.</p>"
		},
		{
			// Datos del poema
			derecha: "3181",
			abajo: "457",
			texto: "<p>Un hombre de obesidad capital,<br>copa de coñac en mano, con la discreción<br>del trasnochado mira los nervios de tabicón<br>deslizarse por los barrancos<br>hasta el corazón de la ciudad.</p>"
		},
		{
			// Datos del poema
			derecha: "6263",
			abajo: "467",
			texto: "<p>A veces quiero huir de aquí<br>a pie por las montañas.<br>Huir solo o a tu lado<br>para no volver.</p>"
		},
		{
			// Datos del poema
			derecha: "8720",
			abajo: "457",
			texto: "<p>Un antiguo cementerio. Cerca colinas, canales y milpas.<br>Su skatepark junto al mercado. Un joven promesa.<br>Siempre sólo promesa. Hermano de clase diferente.<br>Su familia vive a unas cuadras del panteón,<br>“muy lejos de mi zona de confort.”</p>"
		},
		{
			// Datos del poema
			derecha: "10621",
			abajo: "457",
			texto: "<p>La imagen de nuestra nación muere<br>de la vergüenza, querida, se oculta<br>detrás del antifaz del delirio.<br>La marca de nuestra nación,<br>México, a duras penas un lamento.</p>"
		},
		{
			// Datos del poema
			derecha: "11765",
			abajo: "460",
			texto: "<p>Vino tu señor nuestro señor<br>desde más allá de mares montañas muros<br>a contarnos del cielo donde los cuerpos<br>del pecado de ayer a mañana<br>son siempre pesar.</p>"
		},
		{
			// Datos del poema
			derecha: "12914",
			abajo: "650",
			texto: "<p>Al verla tendida sobre el horizonte,<br>su rostro clavado entre las nubes,<br>siendo todos sus milenios en ese instante,<br>me pregunto si piensa en mí.</p>"
		},
		{
			// Datos del poema
			derecha: "14297",
			abajo: "428",
			texto: "<p>¿Por cuánto tiempo las cimas áureas<br>del anciano paisajismo resistirán<br>esta brusca economía<br>del tabicón?</p>"
		},
		{
			// Datos del poema
			derecha: "16633",
			abajo: "380",
			texto: "<p>Sobre el valle de los volcanes<br>toda piedra es un recuerdo que arde.<br>En el subterráneo lacustre rebulle la sangre.<br>Abro los ojos. No estoy solo. Aquí<br>sigues tú, por fortuna. </p>"
		},
		{
			// Datos del poema
			derecha: "17691",
			abajo: "248",
			texto: "<p>Las ondas rayan el aire. Ojo humano tan nuestro<br>no lo nota. Mensajes de cerca a lejos,<br>de vuelta a vuelta. ¿Los escuchas?<br>Se funden con el rumor de esta bestia inmensa y<br>mansa que otros nombran ciudad.</p>"
		},
		{
			// Datos del poema
			derecha: "18017",
			abajo: "518",
			texto: "<p>Las rutas de vuelo<br>sobre la cuenca trazan espirales,<br>como serpientes revuelven<br>el soplido esmeralda de aquellos días<br>cuando las máquinas tenían otro cuerpo.</p>"
		},
		{
			// Datos del poema
			derecha: "19025",
			abajo: "320",
			texto: "<p>Contemplo el espejo del tiempo.<br>Sobre su superficie brilla el pasado que fue luna,<br>el presente que no es más que cráter,<br>un futuro sin carne, sin nombre, sin lugar.</p>"
		},
		{
			// Datos del poema
			derecha: "19872",
			abajo: "303",
			texto: "<p>Olas teje la memoria<br>por el llano de hormigón.<br>Sobre barullo desierto flotamos<br>informes.</p>"
		},
		{
			// Datos del poema
			derecha: "20516",
			abajo: "335",
			texto: "<p>El cielo revienta<br>al filo de la modernidad.<br>A la base los humanos<br>en filita sin sanar.  </p>"
		},
		{
			// Datos del poema
			derecha: "21358",
			abajo: "274",
			texto: "<p>Vivimos cerca de estas torres<br>inconclusas; ellas nos separan de la ciudad<br>de tus abuelos, mis abuelos, sus abuelos,<br>quienes aún nos sujetan cada vez<br>que tiembla.</p>"
		},
		{
			// Datos del poema
			derecha: "22417",
			abajo: "406",
			texto: "<p>Allende las líneas –estas reglas–<br>existieron otros mundos.<br>Hoy todas las líneas conducen<br>al mismo nudo.<br>Tráfico y cerrazón por doquier.  </p>"
		},
		{
			// Datos del poema
			derecha: "23777",
			abajo: "488",
			texto: "<p>He reposado cerca de esta cumbre.<br>He visto la ciudad al límite.<br>He proyectado que es posible<br>lo posible, incluso aquí.<br>Con eso basta.</p>"
		},
		{
			// Datos del poema
			derecha: "26633",
			abajo: "304",
			texto: "<p>Una columna de humo se alza a lo lejos.<br>Avatar de un dios ancestral,<br>padre de toda destrucción.<br>Humor de máquina<br>elegimos aspirar.</p>"
		},
		{
			// Datos del poema
			derecha: "27510",
			abajo: "520",
			texto: "<p>La imaginación no basta<br>para pensar la Ciudad;<br>queda verla crecer<br>más allá de la razón.</p>"
		}
	],
};